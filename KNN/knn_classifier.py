import heapq
from collections import Callable
from typing import List

import numpy as np


class KnnClassifier:
    def __init__(self, distance_function=None):
        self.distance_function = self.euclidean_distance if distance_function is None else distance_function
        self.train_points = []
        self.train_labels = []

    @staticmethod
    def euclidean_distance(point_a: np.ndarray, point_b: np.ndarray) -> float:
        diff = np.subtract(point_a, point_b)
        return np.linalg.norm(diff)

    def fit(self, train_data: List[List[float]], train_labels: List[str]) -> None:
        assert len(train_data) == len(train_labels)
        indices = range(len(train_data))
        self.train_points = list(zip(indices, train_data))
        self.train_labels = list(zip(indices, train_labels))

    def classify(self, point: List[float], k: int) -> str:
        distances = [(self.distance_function(t_point[1], point), t_point[0]) for t_point in self.train_points]
        heapq.heapify(distances)
        closest_points = heapq.nsmallest(k, distances)
        labels = [self.train_labels[p[1]] for p in closest_points]
        return max(set(labels), key=labels.count)[1]

    def predict(self, points: np.ndarray, k: int) -> List[str]:
        results = []
        for point in points:
            results.append(self.classify(point, k))
        return results
