import os
import sys
import json
import logging
from time import time

from data_utils import *
from knn_classifier import KnnClassifier

RESULTS_PATH = "results"


def configure_logging():
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)

    log_formatter = logging.Formatter('%(asctime)s %(message)s')

    logger_file_handler = logging.FileHandler('knn.log', mode="w+")
    logger_file_handler.setFormatter(log_formatter)

    logger_console_handler = logging.StreamHandler(sys.stdout)
    logger_console_handler.setFormatter(log_formatter)

    root_logger.addHandler(logger_file_handler)
    root_logger.addHandler(logger_console_handler)


def run_knn_and_save(classifier: KnnClassifier, test_features: pd.DataFrame, k: int, dist_func: str):
    start_time = time()
    logging.info(f'Classifying for k={k} with {dist_func}...')
    test_labels = classifier.predict(test_features.values, k)
    logging.info(dict(zip(set(test_labels), map(test_labels.count, set(test_labels)))))
    test_result = test_features.copy()
    test_result['Gender'] = test_labels
    logging.info('Writing to file...')
    test_result.to_csv(f'{RESULTS_PATH + os.sep}mytestk{k}{dist_func[0]}.csv')
    logging.info(f'Done in {time() - start_time} seconds.')


def run_and_measure_accuracy(classifier: KnnClassifier, data: pd.DataFrame, k: int, dist_func: str):
    start_time = time()
    logging.info('Splitting and fitting...')
    train_features, train_labels, test_features, test_labels = split_train_test(data, 0.5)
    classifier.fit(train_features.values, train_labels.values)
    logging.info(f'Classifying for k={k} with {dist_func}...')
    test_results = classifier.predict(test_features.values, k)
    accuracy = measure_accuracy(test_labels.values, test_results)
    logging.info(f'Test accuracy: {accuracy * 100}%.')
    logging.info(f'Done in {time() - start_time} seconds.')
    return accuracy


if __name__ == "__main__":
    start_time = time()

    if not os.path.exists(RESULTS_PATH):
        os.makedirs(RESULTS_PATH)

    configure_logging()

    train_data = pd.read_csv('mytrain.csv', low_memory=False)
    test_data = pd.read_csv('mytest.csv', low_memory=False)

    train_features, train_labels = get_features_and_labels(train_data)
    test_features, _ = get_features_and_labels(test_data)

    knn_classifier = KnnClassifier()
    knn_classifier.fit(train_features.values, train_labels.values)

    for k in [1, 7, 19]:
        run_knn_and_save(knn_classifier, test_features, k, "euclidean distance")

    knn_classifier.distance_function = manhattan_distance

    for k in [1, 7, 19]:
        run_knn_and_save(knn_classifier, test_features, k, "manhattan distance")

    knn_classifier.distance_function = hamming_distance

    for k in [1, 7, 19]:
        run_knn_and_save(knn_classifier, test_features, k, "hamming distance")

    accuracies = {}

    knn_classifier.distance_function = knn_classifier.euclidean_distance

    for k in [1, 7, 19]:
        accuracies[f'k={k};Euclidean'] = run_and_measure_accuracy(knn_classifier, train_data, k, "euclidean distance")

    knn_classifier.distance_function = manhattan_distance

    for k in [1, 7, 19]:
        accuracies[f'k={k};Manhattan'] = run_and_measure_accuracy(knn_classifier, train_data, k, "manhattan distance")

    knn_classifier.distance_function = hamming_distance

    for k in [1, 7, 19]:
        accuracies[f'k={1};Hamming'] = run_and_measure_accuracy(knn_classifier, train_data, 1, "hamming distance")

    with open(f'{RESULTS_PATH + os.sep}accuracy_results.json', "w") as f:
        json.dump(accuracies, f, indent=4)

    logging.info(f'Total running time: {time() - start_time} seconds.')

    sys.exit(0)
