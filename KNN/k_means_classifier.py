from typing import List, Callable, Dict

import numpy as np


class KMeansClassifier:
    def __init__(self, n: int, dimension: int = 2, distance_function: Callable[[tuple, tuple], float] = None):
        self.n = n
        self.distance_function = self.euclidean_distance if distance_function is None else distance_function
        self.dimension = dimension
        self.center_points = []
        self.clusters = {}

    @staticmethod
    def euclidean_distance(point_a: tuple, point_b: tuple) -> float:
        diff = np.subtract(point_a, point_b)
        return np.linalg.norm(diff)

    def get_point_cluster(self, point: tuple) -> int:
        for cluster, cluster_points in self.clusters.items():
            if point in cluster_points:
                return cluster
        return -1

    def find_closest_cluster(self, point: tuple) -> int:
        distances = zip(range(self.n), [self.distance_function(point, center) for center in self.center_points])
        closest_cluster = min(distances, key=lambda x: x[1])[0]
        return closest_cluster

    def update_point(self, point: tuple) -> bool:
        closest_cluster = self.find_closest_cluster(point)
        current_cluster = self.get_point_cluster(point)
        if current_cluster != closest_cluster:
            if current_cluster != -1:
                self.clusters[current_cluster].remove(point)
            self.clusters[closest_cluster] = self.clusters.get(closest_cluster, []) + [point]
            return True
        return False

    def update_centers(self) -> None:
        for cluster, cluster_points in self.clusters.items():
            # Updates the center point with the average value of the points that were closest to this center point.
            self.center_points[cluster] = tuple(map(np.mean, zip(*cluster_points)))

    def fit(self, train_data: List[tuple], max_epochs=1000) -> None:
        train_data = set(train_data)  # Verify that there are no duplications in the data points.
        max_value = max([data_point for point in train_data for data_point in point])
        self.center_points = [tuple(np.random.randint(max_value, size=self.dimension)) for _ in range(self.n)]
        not_stable = True
        epoch_count = 0
        while not_stable and epoch_count < max_epochs:
            not_stable = False
            epoch_count += 1
            for point in train_data:
                not_stable = self.update_point(point)
            self.update_centers()

    def predict(self, points: List[tuple]) -> Dict[tuple, int]:
        points = set(points)
        results = {}
        for point in points:
            results[point] = self.find_closest_cluster(point)
        return results
