import numpy as np
import pandas as pd
from scipy.spatial.distance import cdist


def measure_accuracy(labels: list, predictions: list) -> float:
    """
    Measures the accuracy of the prediction given the ground truth and the predictions.
    :param labels The ground truth.
    :param predictions: The predictions.
    :return: The accuracy of the predictions compare to the ground truth.
    """
    assert len(labels) == len(predictions)
    return sum([1 if labels[i] == predictions[i] else 0 for i in range(len(labels))]) / len(labels)


def get_features_and_labels(data: pd.DataFrame) -> (pd.DataFrame, pd.DataFrame):
    """
    Splits a dataframe to the features and the labels when the labels are in the last column and the features are in the
    rest.
    :param data: The data to split
    :return: The data split into features and labels.
    """
    features = data.iloc[:, :-1]
    labels = data.iloc[:, -1]
    return features, labels


def manhattan_distance(point_a: np.ndarray, point_b: np.ndarray) -> float:
    """
    Calculates the manhattan distance between 2 points. I used the scipy implementation since it way faster than mine.
    I added mine in a comment to be sure that I'm filling the exercise requirements.
    :param point_a: Point a.
    :param point_b: Point b.
    :return: The manhattan distance between point_a and point_b.
    """
    # See https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.cdist.html
    assert len(point_a) == len(point_b)
    return cdist([point_a], [point_b], metric='cityblock')[0]
    # If for some reason you insist on manual implementation (the scipy implementation is literally 20 times faster):
    # return sum(abs(point_a[i] - point_b[i]) for i in range(len(point_a)))


def hamming_distance(point_a: np.ndarray, point_b: np.ndarray) -> float:
    """
    Calculates the hamming distance between 2 points. I used the scipy implementation since it way faster than mine.
    I added mine in a comment to be sure that I'm filling the exercise requirements.
    :param point_a: Point a.
    :param point_b: Point b.
    :return: The hamming distance between point_a and point_b.
    """
    # See https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.cdist.html
    assert len(point_a) == len(point_b)
    return cdist([point_a], [point_b], metric='hamming')[0]
    # If for some reason you insist on manual implementation:
    # return sum([1 if point_a[i] != point_b[i] else 0 for i in range(len(point_a))])


def split_train_test(data: pd.DataFrame, train_size: float) -> (pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame):
    """
    Splits a dataframe into train and test and separate the features and the labels.
    :param data: The data to split.
    :param train_size: The relative size of the train part. Should be a number between (0.0, 1.0).
    :return: 4 dataframes in this order: train_features, train_labels, test_features, test_labels
    """
    msk = np.random.rand(len(data)) < train_size
    train_data = data[msk]
    test_data = data[~msk]
    train_features, train_labels = get_features_and_labels(train_data)
    test_features, test_labels = get_features_and_labels(test_data)
    return train_features, train_labels, test_features, test_labels
