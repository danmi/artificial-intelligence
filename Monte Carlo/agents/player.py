from abc import ABC, abstractmethod

from boards import Board


class Player(ABC):

    def __init__(self, name: str, player: str, opponent: str):
        self.name: str = name
        self.player: str = player
        self.opponent: str = opponent
        self.scores_history = []
        self.moves_history = []

    @abstractmethod
    def select_move(self, board: Board) -> int:
        pass

    @abstractmethod
    def reset(self) -> None:
        self.scores_history = []
        self.moves_history = []
