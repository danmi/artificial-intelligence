from agents import Player
from utils import run_input_loop
from boards import ConnectFourBoard


def try_parse_column(value: str) -> bool:
    try:
        if value is not None and 1 <= int(value) <= 7:
            return True
        return False
    except ValueError:
        return False


class HumanPlayer(Player):

    def reset(self) -> None:
        pass

    def __init__(self, name: str, player: str, opponent: str):
        super().__init__(name, player, opponent)

    @staticmethod
    def request_column() -> int:
        return int(run_input_loop("Please select a column:", try_parse_column,
                                  "You must enter a column number between 1 to 7!"))

    def select_move(self, board: ConnectFourBoard) -> int:
        selection = self.request_column()
        while board.is_column_full(selection - 1):
            print('The column you selected is already full!')
            selection = self.request_column()
        return selection
