import os
import random
import pickle
import logging
import numpy as np
from typing import Tuple, Dict, List

from agents import Player

from utils import select_best_move
from boards import ConnectFourBoard


class TDAgent(Player):
    def __init__(self, name: str, player: str, opponent: str, lr: float, gamma: float, exp_rate: float, file: str):
        """
        Creates new model using for playing connect four using temporal difference.
        :param player: The string representing the player on the board.
        :param opponent: The string representing the opponent on the board.
        :param lr: The learning rate.
        :param gamma: The discount rate during the reward feeding.
        :param exp_rate: The exploration rate (i.e. how much the model will choose random option instead of the best
        move it knows so far. This parameter should be equal to 1 if the agent is not in training mode for best result.
        :param file: The file where the model will be stored.
        during the training phase.
        """
        super().__init__(name, player, opponent)
        self.alpha: float = lr
        self.exploration_rate: float = exp_rate
        self.gamma: float = gamma
        self.file = file
        self.states: List[int] = []
        self.states_value: Dict[int, float] = {}
        if os.path.exists(file):
            self.load_model()

    @staticmethod
    def select_random_move(board: ConnectFourBoard) -> int:
        return random.choice(list(board.get_free_columns()))

    def feed_reward(self, reward: float):
        """
        Feed the reward backwards to the previous states, gradually decreasing using the following formula:
        value[s_t] = value[s_t] + α*(reward + γ*values[s_t'] - values[s_t]) where value[s_t] is the current value of
        the state, reward is the reward given, γ is the decay rate and value[s_t'] is the value of the new state.
        :param reward: the reward given based on the game result.
        """
        reversed_states = list(reversed(self.states))
        for idx, state in enumerate(reversed_states):
            old_value = self.states_value.get(state, 0)
            new_state = reversed_states[idx + 1] if idx + 1 < len(reversed_states) else None
            new_value = self.states_value.get(new_state, 0) if new_state else 0
            self.states_value[state] = old_value + self.alpha * (reward + self.gamma * new_value - old_value)

    def select_move(self, board: ConnectFourBoard):
        """
        Selects the next move. First, a random number is selected between 0 and 1. If the number is smaller than the
        exploration rate - the agent is exploring its next move at random. Otherwise, the agent is exploiting its
        previous knowledge using his model, and select the moves with the higher score.
        :param board: The current state of the board.
        :return:
        """
        if np.random.uniform(0, 1) < self.exploration_rate:
            # Exploration
            selected_column = self.select_random_move(board)
        else:
            # Exploitation
            # List containing tuples of column and its following reward.
            rewards: List[Tuple[int, float]] = []
            for column in board.get_free_columns():
                next_board: ConnectFourBoard = board.copy()
                next_board.drop_disc(column + 1, self.player)
                next_board_hash = next_board.get_hash()
                reward = self.states_value.get(next_board_hash, 0)
                rewards.append((column, reward))
            selected_column = select_best_move(rewards)
        # Shifting from array indexing to column indexing
        return selected_column + 1

    def store_model(self) -> None:
        """
        Stores the model in a pickle dump.
        """
        logging.info(f"Storing {self.name}'s model...")
        with open(self.file, 'wb') as f:
            pickle.dump(self.states_value, f)
        logging.info(f"Finished storing {self.name}'s model.")

    def load_model(self) -> None:
        """
        Loads a model from a pickle dump.
        """
        logging.info(f"Loading {self.name}'s model...")
        with open(self.file, 'rb') as f:
            self.states_value = pickle.load(f)
        logging.info(f"Finished loading {self.name}'s model.")

    def reset(self) -> None:
        super().reset()
        self.states = []
