from agents import AIAgent
from heuristics.heuristic import Heuristic
from utils import select_best_move
from boards.connect_four_board import ConnectFourBoard


class DebuggingAgent(AIAgent):
    def __init__(self, name: str, player: str, opponent: str, moves: list, heuristic: Heuristic):
        """
        Initializes new Debugging Agent.
        :param name: The name of the agent.
        :param player: The string representing the agent on the board.
        :param opponent: The string representing the opponent on the board.
        :param moves: The moves that the agent should do.
        :param heuristic: The heuristic the agent should use after it finished to use all the provided moves.
        """
        super().__init__(name, player, opponent, heuristic)
        self.moves = moves
        self.moves_count = 0
        self.heuristic = heuristic

    def reset(self) -> None:
        """
        Reset the agent to its starting position.
        """
        super().reset()
        self.moves_count = 0

    def select_move(self, board: ConnectFourBoard) -> int:
        if self.moves_count < len(self.moves):
            next_move = self.moves[self.moves_count]
            self.moves_count += 1
        else:
            gains = self.calculate_scores(board)
            next_move = select_best_move(gains) + 1
        return next_move
