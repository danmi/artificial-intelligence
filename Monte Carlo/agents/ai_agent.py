from agents import Player
from utils import select_best_move
from heuristics import Heuristic
from boards import ConnectFourBoard


class AIAgent(Player):
    """
    A class representing an AI player that uses a heuristic for selecting its moves.
    """

    def __init__(self, name: str, player: str, opponent: str, heuristic: Heuristic):
        """
        Initializes new instance of AI Agent.
        :param name: The name of the agent.
        :param player: The string representing the agent on the board..
        :param opponent: The string representing the opponent on the board.
        :param heuristic: The heuristic the AI agent uses in order to determines its next move.
        """
        super().__init__(name, player, opponent)
        self.heuristic = heuristic

    def calculate_scores(self, board: ConnectFourBoard):
        """
        Calculates the score for each column. If the column has free spaces then the score is calculated using the
        heuristic, else the column will be dropped from the scores list.
        :return: A list containing tuples of column number and the score for this columns.
        """
        scores = []
        for column in range(7):
            if board.is_column_full(column):
                score = -1
            else:
                score = self.heuristic.calculate_heuristic(board.copy(), column, self.player, self.opponent)
            if score >= 0:
                scores.append((column, score))
        self.scores_history.append(scores)
        return scores

    def reset(self) -> None:
        """
        Resets the moves history and the scores history of the agent.
        """
        super().reset()

    def select_move(self, board: ConnectFourBoard) -> int:
        """
        Selects a move based on the agent's heuristic.
        :param board: The current state of the board.
        :return: The selected move.
        """
        scores = self.calculate_scores(board)
        # Just converting from the array index (that starts with 0) and the columns index
        next_move = select_best_move(scores) + 1
        self.moves_history.append(next_move)
        return next_move
