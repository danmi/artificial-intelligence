import logging
from agents import TDAgent, Player
from boards import MoveResult, ConnectFourBoard
from config import PRINT_INTERVAL


class TDTrainer:
    """
    A class for training an agent using Temporal Difference. This class uses 2 TDAgents for playing against each other
    and created a model for each player.
    """

    def __init__(self, board: ConnectFourBoard, player1: TDAgent, player2: Player,
                 win_reward: float, loose_reward: float, tie_reward: float):
        """
        Initializes new TDTrainer.
        :param board: The board to use.
        :param player1: The first TDPlayer.
        :param player2: The second TDPlayer.
        :param win_reward: The reward the players are getting for winning a game.
        :param loose_reward: The reward the players are getting for loosing a game.
        :param tie_reward: The reward the players are getting when a game is ending with a tie.
        """
        self.board: ConnectFourBoard = board
        self.player1: TDAgent = player1
        self.player2: Player = player2
        self.win_reward = win_reward
        self.loose_reward = loose_reward
        self.tie_reward = tie_reward

    def play_game(self, first: bool = True) -> str:
        """
        Plays a single game between the 2 TDPlayer.
        :param first: Determines whether player1 is first or not.
        :return: The winner in the game.
        """
        self.board.reset()
        self.player1.reset()
        self.player2.reset()
        players = [self.player1, self.player2]
        turn = 0 if first else 1

        while not self.board.is_full():
            selection = players[turn].select_move(self.board)
            move_result = self.board.drop_disc(selection, players[turn].player)
            if players[turn].name == self.player1.name:
                players[turn].states.append(self.board.get_hash())
            if move_result == MoveResult.WIN:
                return players[turn].name

            turn = 0 if turn else 1
        return 'tie'

    def give_reward(self, winner: str):
        """
        Gives reward for each player according to the winner in the game. The reward are given using the player's
        feed_reward method which is using the Temporal Difference formula.
        :param winner: The player who won the last game.
        """
        if winner == self.player1.name:
            self.player1.feed_reward(self.win_reward)
        elif winner == self.player2.name:
            self.player1.feed_reward(self.loose_reward)
        elif winner == 'tie':
            self.player1.feed_reward(self.tie_reward)

    def store_models(self):
        self.player1.store_model()

    def train(self, rounds: int, first: bool = True):
        """
        Runs n games between the 2 TDAgents and stores their model at the end.
        :param rounds: How many rounds to run.
        :param first: Whether player1 will be first or not.
        """
        logging.info('Training started...')
        for i in range(rounds):
            if not (i - 1) % 1000 and i - 1:
                logging.info(f'Train round {i - 1} Finished...')
                self.store_models()
            winner = self.play_game(first)
            self.give_reward(winner)
            # Change who is first is the next game in order to allow both players to learn how to play when they are
            # first and when they are not.
            first = not first
        logging.info("Finished training.")
        self.store_models()
