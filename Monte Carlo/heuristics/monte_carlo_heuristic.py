from typing import Optional

from agents import Player, AIAgent
from game_runner import GameRunner
from boards import ConnectFourBoard
from heuristics import ConstantHeuristic, Heuristic


class MonteCarloHeuristic(Heuristic):
    def __init__(self, rounds: int):
        self.me: Optional[Player] = None
        self.random_opponent: Optional[Player] = None
        self.rounds: int = rounds

    def __create_players(self, me: str, opponent: str):
        if not self.me:
            self.me = AIAgent(me, me, opponent, ConstantHeuristic(7))
        if not self.random_opponent:
            self.random_opponent = AIAgent(opponent, opponent, me, ConstantHeuristic(7))

    def calculate_heuristic(self, board: ConnectFourBoard, column: int, player: str, opponent: str):
        self.__create_players(player, opponent)
        if board.add_to_column(column, self.me.player) == -1:
            return -1
        game_runner = GameRunner(board, self.random_opponent, self.me, False)
        results = game_runner.run_games_batch(self.rounds, alternate=False)
        board.remove_from_column(column)
        return results[f'{self.random_opponent.name} First'][self.me.name] / self.rounds
