from heuristics import Heuristic
from boards import ConnectFourBoard


class ConstantHeuristic(Heuristic):
    def __init__(self, constant_score: float):
        self.constant_score: float = constant_score

    def calculate_heuristic(self, board: ConnectFourBoard, column: int, player: str, opponent: str) -> float:
        return self.constant_score
