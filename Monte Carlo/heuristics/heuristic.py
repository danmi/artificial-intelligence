from abc import ABC, abstractmethod

from boards import ConnectFourBoard


class Heuristic(ABC):

    @abstractmethod
    def calculate_heuristic(self, board: ConnectFourBoard, column: int, player: str, opponent: str) -> float:
        pass
