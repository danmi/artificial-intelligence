import math
from typing import Dict

from heuristics import Heuristic
from boards import ConnectFourBoard
from utils import boost_n_free_in_both_direction


class StandardHeuristic(Heuristic):
    def __init__(self, scores: Dict[str, float]):
        self.scores = scores

    def calculate_gain(self, board: ConnectFourBoard, column: int, player: str) -> float:
        row = board.add_to_column(column, player)
        # if row == -1:
        #     return -1

        c = {
            'horizontal': board.count_horizontal(row, column, player),
            'vertical': board.count_vertical(row, column, player),
            'diagonal': board.count_major_diagonal(row, column, player),
            'minor_diagonal': board.count_minor_diagonal(row, column, player)
        }

        # I'm using a dictionary to save the gain at every direction and don't sum it all to a single
        # variable in order to allow easier understanding of the final score selection at each step.
        gains = {}

        for value in c.values():
            if value[0] >= 4:  # Means that we win if we select this column.
                board.remove_from_column(column)
                return math.inf

        # Checks if it creates a block of 3 that is not blocked at both direction. This will result in a very high
        # score since it means that the victory is guaranteed if the opponent does not win in the next move.
        boost_n_free_in_both_direction(3, c, gains, self.scores['three_both_directions'])

        # Checks if it creates a block of 2 that is not blocked at both direction. This will result in a slight boost in
        # overall gain.
        boost_n_free_in_both_direction(2, c, gains, self.scores['two_both_directions'])

        # Checks if it creates a block of 1 that is not blocked at both direction. This case will be mostly at the
        # beginning of the game, so it will boost the gains only by a few points.
        boost_n_free_in_both_direction(2, c, gains, self.scores['one_both_directions'])

        for direction, counts in c.items():
            gain = 0
            total_free_spaces_around = counts[1] + counts[2]
            # If the free spaces are balanced on both sides then another boost will be given.
            balance_boost = True if abs(counts[1] - counts[2]) <= 1 else False
            if total_free_spaces_around == 4 - counts[0]:
                gain = self.scores['4-n'] * counts[0]
            elif total_free_spaces_around == 5 - counts[0]:
                gain = self.scores['5-n'] * counts[0]
            elif total_free_spaces_around == 6 - counts[0]:
                gain = self.scores['6-n'] * counts[0]
            elif total_free_spaces_around == 7 - counts[0]:
                gain = self.scores['7-n'] * counts[0]

            boost = self.scores['balance_boost'] if balance_boost else 0
            gains[direction] = gains.get(direction, 0) + gain + boost

        board.remove_from_column(column)

        return sum(gains.values())

    def calculate_total_score(self, board: ConnectFourBoard, column: int, player: str, opponent: str) -> float:
        agent_score = self.calculate_gain(board.copy(), column, player)
        opponent_score = self.calculate_gain(board.copy(), column, opponent)
        return self.scores['agent_coef'] * agent_score + self.scores['opponent_coef'] * opponent_score

    def calculate_heuristic(self, board: ConnectFourBoard, column: int, player: str, opponent: str):
        return self.calculate_total_score(board, column, player, opponent)
