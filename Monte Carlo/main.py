import sys
import logging
import traceback
from time import time
from multiprocessing import Pool
from typing import Dict, Optional

from config import *
from td_trainer import TDTrainer
from game_runner import GameRunner
from boards import ConnectFourBoard
from runner_utils import create_players
from utils import run_input_loop, is_positive_integer, LoggingManager

logging_manager: Optional[LoggingManager] = None


def run_n_games(a1_type: str, a2_type: str) -> None:
    """
    Runs n games between 2 agents and prints the results.
    :param a1_type: The type of the first agent.
    :param a2_type: The type of the second agent.
    """
    n = run_input_loop("How many games do you want to run?", is_positive_integer, "Please enter a positive integer")
    print("Note: Only a summary of the result will be shown. The full games will be logged in 'connect_four.log'")

    n = int(n)

    board = ConnectFourBoard()

    logging_manager.set_file_handler()

    player1, player2 = create_players(a1_type, f'{PLAYER_NAMES[a1_type]} (1)', a2_type, f'{PLAYER_NAMES[a2_type]} (2)')

    game_runner = GameRunner(board, player1, player2)
    print("Games started...")
    results = game_runner.run_games_batch(n, log=True)
    first_player_success_first = n / 2 - results[f'{player1.name} First'][f'{player2.name}']
    first_player_success_second = n / 2 - results[f'{player2.name} First'][f'{player2.name}']
    first_total_success = first_player_success_first + first_player_success_second

    second_player_success_first = n / 2 - results[f'{player2.name} First'][f'{player1.name}']
    second_player_success_second = n / 2 - results[f'{player1.name} First'][f'{player1.name}']
    second_total_success = second_player_success_first + second_player_success_second

    print(f'{player1.name} success rate when first: {first_player_success_first / (n / 2)}')
    print(f'{player1.name} success rate when second: {first_player_success_second / (n / 2)}')
    print(f'{player1.name} total success rate: {first_total_success / n}')

    print(f'{player2.name} success rate when first: {second_player_success_first / (n / 2)}')
    print(f'{player2.name} success rate when second: {second_player_success_second / (n / 2)}')
    print(f'{player2.name} total Success rate: {second_total_success / n}')

    print('Detailed statistics:')
    for key, value in results.items():
        print(key)
        for winner, count in value.items():
            print(f'\t{winner}: {count}')


def single_auto_game(a1_type: str, a2_type: str) -> None:
    """
    Runs a single game between 2 agents.
    :param a1_type: The type of the first agent.
    :param a2_type: The type of the second agent.
    :return:
    """
    board = ConnectFourBoard()

    player1, player2 = create_players(a1_type, f'{PLAYER_NAMES[a1_type]} (1)', a2_type, f'{PLAYER_NAMES[a2_type]} (2)')

    logging_manager.set_all_handlers()

    game_runner = GameRunner(board, player1, player2)

    game_runner.run_single_game()


def single_manual_game(player_type: str) -> None:
    """
    Runs a game between a human player and an agent.
    :param player_type: The type of the agent.
    """
    is_human_first = run_input_loop('Do you want to be first? (y/n)', lambda x: x in ['y', 'n'],
                                    "Please select 'y' or 'n'")
    board = ConnectFourBoard()

    logging_manager.set_all_handlers()

    human_player, ai_agent = create_players("human", player_name, player_type, PLAYER_NAMES[player_type])

    game_runner = GameRunner(board, human_player if is_human_first == 'y' else ai_agent,
                             ai_agent if is_human_first == 'y' else human_player)

    game_runner.run_single_game()


def control_universe() -> None:
    """
    ToDo: Implement.
    """
    raise NotImplementedError


def run_mc_game(x: int) -> str:
    """
    Runs a single game between the standard AI agent and the Monte Carlo agent.
    :param x: The game number (for logging).
    :return: The winner of the game.
    """
    board = ConnectFourBoard()
    smart_agent, monte_carlo_player = create_players('ai', AI_AGENT_NAME, 'mc', MC_AGENT_NAME)
    print(f'Starting game number {x}')
    game_runner = GameRunner(board, smart_agent, monte_carlo_player)
    winner = game_runner.run_single_game()
    return winner


def hill_climbing(score: str) -> Dict[str, float]:
    """
    Performs a hill climbing on a specific score in constant steps. This functions runs n games in a loop and determines
    the success rate. If the success rate is higher than the one before then the climbing continue, otherwise the step
    is cut by a half. The function continues until the step is smaller than 1.
    All the scores during the hill climbing and their corresponding success rates are returned in a dictionary.
    the success
    :param score: The score to change during the hill climbing.
    :return: A dictionary containing all the scores and their success rates.
    """
    n = int(run_input_loop("How many games do you want to run at each step?", is_positive_integer,
                           "Please enter a positive integer"))
    step = int(run_input_loop("What should be the initial step?", is_positive_integer,
                              "Please enter a positive integer"))
    initial_score = SCORES[score]
    best_success_rate = run_n_mc_games(n)
    initial_success_rate = best_success_rate
    current_success_rate = best_success_rate
    success_rates = {score: current_success_rate}
    while int(step) > 1:
        SCORES[score] += step
        current_success_rate = run_n_mc_games(n)
        success_rates[score] = current_success_rate
        if current_success_rate > best_success_rate:
            best_success_rate = current_success_rate
        if current_success_rate <= best_success_rate:
            SCORES[score] -= step
            step /= 2
        print(f'{step=}')
    print(f'Initial score: {initial_score}')
    print(f'Final score: {SCORES[score]}')
    print(f'Initial success rate: {initial_success_rate}')
    print(f'Final success rate: {best_success_rate}')
    return success_rates


def run_n_mc_games(n: int = None) -> float:
    """
    Runs n games between the standard AI agent and the Monte Carlo agent. This functions runs the games in parallel
    using a process pull.
    :param n: How many games to run.
    :return: The success rate of the AI agent (a number between 0 and 1).
    """
    if not is_positive_integer(str(n)):
        n = int(run_input_loop("How many games do you want to run?", is_positive_integer,
                               "Please enter a positive integer"))
    with Pool(processes=NUM_OF_PROCESSES) as process_pool:
        results = process_pool.map(run_mc_game, range(n))
    print(f'AI won: {results.count(AI_AGENT_NAME)}')
    print(f'MC won: {results.count(MC_AGENT_NAME)}')
    print(f'Tie: {results.count("tie")}')
    print(f'Success rate: {results.count(AI_AGENT_NAME) / len(results)}')
    return results.count(AI_AGENT_NAME) / len(results)


def train_td_model(agent_type: str) -> None:
    """
    Trains a model using a Temporal Difference method. The parameters for the learning can be configured in the config
    file.
    """
    n = int(run_input_loop("How many rounds do you want in the training?", is_positive_integer,
                           "Please enter a positive integer"))
    logging_manager.set_formatted_console_handler()
    board = ConnectFourBoard()
    player1 = TDAgent("Player1", 'X', 'O', TD_LEARNING_RATE, TD_GAMMA, TD_EXPLORATION_RATE, 'player1.pickle')
    player2 = PLAYERS[agent_type]("Player2", 'O', 'X')
    trainer = TDTrainer(board, player1, player2, 1, -1, 0)
    trainer.train(n)


def welcome_prompt(user: str) -> None:
    """
    Prints a welcome prompt with all the available options to the user.
    :param user: The name of the user.
    """
    # os.system('cls')
    print(f"Welcome {user}! What would you like to do?")
    print("0. Play against the Monte Carlo")
    print("1. Let the ai agent play {n} games against the MC agent (multi-threaded)")
    print("2. Hill climbing on a random score weight")
    print("3. Play against the TD agent.")
    print("4. Train the TD agent against random agent")
    print("5. Train the TD agent against the ai agent")
    print("6. Let the TD agent play {n} games against a random agent")
    print("7. Let the TD agent play {n} games against the ai agent")
    print("8. Play against the ai agent")
    print("9. Play against a random agent (why would anybody want to do that?!)")
    print("10. Watch the ai agent playing against a random agent")
    print("11. Watch the ai agent playing against himself")
    print("12. Let the ai agent play {n} games against a random agent")
    print("13. Let the ai agent play {n} games against himself")
    print("14. Control the universe! HA HA HA (evil laughter)")
    print("15. Exit")


if __name__ == "__main__":
    start_time = time()
    logging_manager: LoggingManager = LoggingManager(logging.getLogger())

    player_name = input("Hi, what's your name? ")
    if player_name == "":
        player_name = "someone"
    # player_name = "Daniel"

    options = {
        '0': partial(single_manual_game, 'mc'),
        '1': run_n_mc_games,
        '2': partial(hill_climbing, 'two_both_directions'),
        '3': partial(single_manual_game, 'td'),
        '4': partial(train_td_model, 'random'),
        '5': partial(train_td_model, 'ai'),
        '6': partial(run_n_games, 'td', 'random'),
        '7': partial(run_n_games, 'td', 'ai'),
        '8': partial(single_manual_game, 'ai'),
        '9': partial(single_manual_game, 'random'),
        '10': partial(single_auto_game, 'ai', 'random'),
        '11': partial(single_auto_game, 'ai', 'ai'),
        '12': partial(run_n_games, 'ai', 'random'),
        '13': partial(run_n_games, 'ai', 'ai'),
        '14': control_universe,
        '15': sys.exit
    }

    while True:
        selection = ""
        while selection not in options.keys():
            logging_manager.reset_handlers()
            welcome_prompt(player_name)
            selection: str = input()
            if selection not in options.keys():
                print("Please select an option between 1 to 8")
        try:
            options[selection]()
        except NotImplementedError as e:
            print(type(e).__name__)
        except Exception as e:
            print("Error!")
            print(type(e).__name__)
            print(str(traceback.format_exc()))

        input("Press Enter to start over...")
