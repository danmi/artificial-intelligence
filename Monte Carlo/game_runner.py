import logging
from attrdict import AttrDict

from agents import Player
from boards.connect_four_board import ConnectFourBoard, MoveResult


class GameRunner:
    def __init__(self, board: ConnectFourBoard, player1: Player, player2: Player, logging: bool = True):
        self.starting_board = board
        self.board = board.copy()
        self.player1 = player1
        self.player2 = player2
        self.logging = logging

    def create_result_dict(self):
        return AttrDict({
            f'{self.player1.name} First':
                {
                    self.player1.name: 0,
                    self.player2.name: 0,
                    f'tie': 0
                },
            f'{self.player2.name} First':
                {
                    self.player1.name: 0,
                    self.player2.name: 0,
                    f'tie': 0
                }
        })

    def reset_board(self):
        self.board = self.starting_board.copy()

    def run_single_game(self) -> str:
        self.reset_board()
        players = [self.player1, self.player2]

        turn = 0
        if self.logging:
            logging.info(f"Starting game: {self.player1.name} VS {self.player2.name}")
            self.board.log_board()

        while True and not self.board.is_full():
            selection = players[turn].select_move(self.board)

            if self.logging:
                # For debugging purposes
                if players[turn].scores_history:
                    logging.debug(players[turn].scores_history[-1])

            move_result = self.board.drop_disc(selection, players[turn].player)
            if self.logging:
                logging.info(f'{players[turn].name} - {players[turn].player}: {selection}')
                self.board.log_board()

            if move_result == MoveResult.WIN:
                if self.logging:
                    logging.info(f'{players[turn].name} won')
                return players[turn].name

            turn = 0 if turn else 1
        if self.logging:
            logging.info("Tie!")
        return 'tie'

    def run_games_batch(self, rounds: int, alternate: bool = True, log: bool = False) -> dict:
        results = self.create_result_dict()
        for i in range(rounds):
            winner = self.run_single_game()
            results[f'{self.player1.name} First'][winner] += 1
            if alternate:
                self.player1, self.player2 = self.player2, self.player1
        return results
