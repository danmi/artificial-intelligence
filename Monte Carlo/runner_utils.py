import logging
from typing import Callable

from agents import Player
from config import PLAYERS
from game_runner import GameRunner


def run_until(game_runner: GameRunner, player1: Player, player2: Player, condition: Callable[[str], bool]) -> None:
    winner = None
    while condition(winner):
        winner = game_runner.run_single_game()
        logging.info(f'{player1.name} moves {player1.moves_history}')
        logging.info(f'{player2.name} moves {player2.moves_history}')
        player1.reset()
        player2.reset()
    print(winner)


def create_players(player1_type, player1_name, player2_type, player2_name) -> (Player, Player):
    player1 = PLAYERS[player1_type](player1_name, 'X', 'O')
    player2 = PLAYERS[player2_type](player2_name, 'O', 'X')
    return player1, player2
