from functools import partial

from attrdict import AttrDict

from agents import AIAgent, HumanPlayer, TDAgent
from heuristics import StandardHeuristic, ConstantHeuristic
from heuristics.monte_carlo_heuristic import MonteCarloHeuristic

NUM_OF_PROCESSES = 5
PRINT_INTERVAL = 100
MONTE_CARLO_ROUNDS = 500
AI_AGENT_NAME = 'AI Agent'
RANDOM_AGENT_NAME = 'Random Agent'
MC_AGENT_NAME = 'MC Agent'
TD_AGENT_NAME = 'TD Agent'

TD_LEARNING_RATE = 0.2
TD_GAMMA = 0.9
TD_EXPLORATION_RATE = 0.3

PLAYER_NAMES = {
    'ai': AI_AGENT_NAME,
    'random': RANDOM_AGENT_NAME,
    'mc': RANDOM_AGENT_NAME,
    'td': TD_AGENT_NAME
}

SCORES = {
    'three_both_directions': 1000,
    'two_both_directions': 300,
    'one_both_directions': 100,
    'balance_boost': 20,
    '7-n': 80,
    '6-n': 60,
    '5-n': 40,
    '4-n': 20,
    'agent_coef': 1,
    'opponent_coef': 1
}

HEURISTICS = AttrDict({
    'standard': StandardHeuristic(SCORES),
    'constant': ConstantHeuristic(7),
    'mc': MonteCarloHeuristic(MONTE_CARLO_ROUNDS)
})

PLAYERS = {
    'ai': partial(AIAgent, heuristic=HEURISTICS.standard),
    'random': partial(AIAgent, heuristic=HEURISTICS.constant),
    'mc': partial(AIAgent, heuristic=HEURISTICS.mc),
    'human': HumanPlayer,
    'td': partial(TDAgent, lr=TD_LEARNING_RATE, gamma=TD_GAMMA, exp_rate=0, file='player1.pickle')
}

