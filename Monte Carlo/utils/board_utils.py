def move_right_up(x, y):
    return x - 1, y + 1


def move_left_up(x, y):
    return x - 1, y - 1


def move_right_down(x, y):
    return x + 1, y + 1


def move_left_down(x, y):
    return x + 1, y - 1


def move_right(x, y):
    return x, y + 1


def move_left(x, y):
    return x, y - 1


def move_up(x, y):
    return x - 1, y


def move_down(x, y):
    return x + 1, y
