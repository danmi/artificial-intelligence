from logging import Formatter
from colorama import init, Fore


class ConnectFourColorFormatter(Formatter):
    def __init__(self, fmt=None, player1: str = 'X', player2: str = 'O', color1: int = Fore.BLUE,
                 color2: int = Fore.RED):
        self.player1 = f' {player1} '
        self.player2 = f' {player2} '
        self.color1 = color1
        self.color2 = color2

        super(ConnectFourColorFormatter, self).__init__(fmt)

    def format(self, record):
        init()
        record.message = record.getMessage()
        record.message = record.message.replace(self.player1, self.color1 + self.player1 + Fore.RESET)
        record.message = record.message.replace(self.player2, self.color2 + self.player2 + Fore.RESET)
        if self.usesTime():
            record.asctime = self.formatTime(record, self.datefmt)
        s = self.formatMessage(record)
        if record.exc_info:
            # Cache the traceback text to avoid converting it multiple times
            # (it's constant anyway)
            if not record.exc_text:
                record.exc_text = self.formatException(record.exc_info)
        if record.exc_text:
            if s[-1:] != "\n":
                s = s + "\n"
            s = s + record.exc_text
        if record.stack_info:
            if s[-1:] != "\n":
                s = s + "\n"
            s = s + self.formatStack(record.stack_info)
        return s
