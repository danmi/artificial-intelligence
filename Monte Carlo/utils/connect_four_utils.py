import random


def select_best_move(scores: list) -> int:
    """
    Select a move based on the following heuristic: Select move with the lowest cost. If there are multiple options with
    the same cost, select one randomly.
    :param scores: A list containing tuples of (index, score).
    :return: One of the indices with the minimum cost.
    """
    sorted_scores = sorted(scores, key=lambda x: -x[1])
    max_score = sorted_scores[0][1]
    max_score_indices = [idx for idx, cost in sorted_scores if cost == max_score]
    return random.choice(max_score_indices)


def is_n_not_blocked_in_both_direction(n: int, count: int, a: int, b: int) -> bool:
    # Look for 3 in a row that are not blocked at both direction:
    if count == n and a >= 4 - n and b >= 4 - n:
        return True
    return False


def boost_n_free_in_both_direction(n: int, c: dict, gains: dict, boost: float) -> None:
    for direction, counts in c.items():
        if is_n_not_blocked_in_both_direction(n, *counts):
            gains[direction] = gains.get(direction, 0) + boost
