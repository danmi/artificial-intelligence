import sys
import logging

from utils.connect_four_log_formatter import ConnectFourColorFormatter


class LoggingManager:
    def __init__(self, root_logger: logging.Logger):
        self.root_logger = root_logger
        self.root_logger.setLevel(logging.INFO)

        self.logger_handlers = []
        log_formatter = logging.Formatter('%(asctime)s %(message)s')
        colored_log_formatter = ConnectFourColorFormatter()

        self.logger_file_handler = logging.FileHandler('connect_four.log', mode="w+")
        self.logger_file_handler.setFormatter(log_formatter)
        self.logger_handlers.append(self.logger_file_handler)

        self.logger_console_handler = logging.StreamHandler(sys.stdout)
        self.logger_console_handler.setFormatter(colored_log_formatter)
        self.logger_handlers.append(self.logger_console_handler)

        self.logger_formatted_console_handler = logging.StreamHandler(sys.stdout)
        self.logger_formatted_console_handler.setFormatter(log_formatter)
        self.logger_handlers.append(self.logger_formatted_console_handler)

    def set_all_handlers(self):
        self.set_handlers(self.logger_console_handler, self.logger_file_handler)

    def reset_handlers(self) -> None:
        for handler in self.root_logger.handlers:
            self.root_logger.removeHandler(handler)

    def set_handlers(self, *handlers) -> None:
        self.reset_handlers()
        for handler in handlers:
            self.root_logger.addHandler(handler)

    def set_console_handler(self):
        self.set_handlers(self.logger_console_handler)

    def set_formatted_console_handler(self):
        self.set_handlers(self.logger_formatted_console_handler)

    def set_file_handler(self):
        self.set_handlers(self.logger_file_handler)
