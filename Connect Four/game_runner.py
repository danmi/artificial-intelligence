import logging
from agents.player import Player
from boards.connect_four_board import ConnectFourBoard, MoveResult


class GameRunner:
    def __init__(self, board: ConnectFourBoard, player1: Player, player2: Player):
        self.board = board
        self.player1 = player1
        self.player2 = player2

    def create_result_dict(self):
        return {
            f'{self.player1.name} First':
                {
                    self.player1.name: 0,
                    self.player2.name: 0,
                    f'tie': 0
                },
            f'{self.player2.name} First':
                {
                    self.player1.name: 0,
                    self.player2.name: 0,
                    f'tie': 0
                }
        }

    def run_single_game(self) -> str:
        self.board.reset()
        players = [self.player1, self.player2]

        turn = 0
        logging.info(f"Starting game: {self.player1.name} VS {self.player2.name}")
        self.board.log_board()

        while not self.board.is_full():
            selection = players[turn].select_move(self.board)

            # For debugging purposes
            if players[turn].scores_history:
                logging.debug(players[turn].scores_history[-1])

            move_result = self.board.drop_disc(selection, players[turn].player)
            logging.info(f'{players[turn].name} - {players[turn].player}: {selection}')
            self.board.log_board()

            if move_result == MoveResult.WIN:
                logging.info(f'{players[turn].name} won')
                return players[turn].name

            turn = 0 if turn else 1

        logging.info("Tie!")
        return 'tie'

    def run_games_batch(self, rounds: int, alternate=True, log=False) -> dict:
        results = self.create_result_dict()
        for i in range(rounds):
            logging.info(f"Starting game {i}")
            if not i % 10:
                print(f'{i} finished.')
            winner = self.run_single_game()
            if log:
                logging.info(f'{self.player1.name} moves {self.player1.moves}')
                logging.info(f'{self.player2.name} moves {self.player2.moves}')
            self.player1.reset()
            self.player2.reset()
            results[f'{self.player1.name} First'][winner] += 1
            if alternate:
                self.player1, self.player2 = self.player2, self.player1
        print(f'{rounds} finished.')
        return results
