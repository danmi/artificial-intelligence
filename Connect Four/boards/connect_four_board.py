import logging
import numpy as np
from enum import Enum
from boards.board import Board
from utils.board_utils import *


class MoveResult(Enum):
    WIN = 1
    OK = 2
    ILLEGAL = 3


class ConnectFourBoard(Board):
    def __init__(self):
        Board.__init__(self, (6, 7), np.dtype("<U1"), " ")
        self.empty = self.zero
        self.board[:] = self.empty

    def reset(self):
        self.board[:] = self.empty

    def is_full(self):
        return np.all([self.board[row, column] != self.empty for row, column in np.ndindex(self.board.shape)])

    def drop_disc(self, selected_column: int, player: str) -> MoveResult:
        if 0 < selected_column <= self.board.shape[1]:
            column = selected_column - 1
            row = self.add_to_column(column, player)
            if row != -1:
                if self.is_four_connected(row, column, player):
                    return MoveResult.WIN
            else:
                return MoveResult.ILLEGAL
        else:
            return MoveResult.ILLEGAL
        return MoveResult.OK

    def add_to_column(self, column: int, player: str) -> int:
        free_places = list(self.board.T[column]).count(" ")
        if free_places < 1:
            return -1
        self.board.T[column][free_places - 1] = player
        return free_places - 1

    def is_column_full(self, column: int) -> bool:
        free_places = list(self.board.T[column]).count(" ")
        return free_places < 1

    def remove_from_column(self, column: int) -> None:
        free_places = list(self.board.T[column]).count(self.empty)
        self.board.T[column][free_places] = self.empty

    def is_n_in_a_row(self, line: int, n: int) -> bool:
        for i in range(len(self.board[line]) - n + 1):
            window = self.board[line][i:i + n]
            if len(set(window)) == 1 and ' ' not in window:
                return True
        return False

    def is_four_connected(self, x: int, y: int, player: str) -> bool:
        if self.count_horizontal(x, y, player)[0] >= 4 or \
                self.count_vertical(x, y, player)[0] >= 4 or \
                self.count_major_diagonal(x, y, player)[0] >= 4 or \
                self.count_minor_diagonal(x, y, player)[0] >= 4:
            return True
        return False

    # <editor-fold desc="counts">

    def count_in_direction(self, x: int, y: int, move_func, player: str) -> (int, int):
        count = 0
        free_count = 0
        x, y = move_func(x, y)
        while 0 <= x < self.board.shape[0] and 0 <= y < self.board.shape[1]:
            if self.board[x, y] == player:
                count += 1
                x, y = move_func(x, y)
            else:
                break

        while 0 <= x < self.board.shape[0] and 0 <= y < self.board.shape[1]:
            if self.board[x, y] == self.empty:
                free_count += 1
                x, y = move_func(x, y)
            else:
                break

        return count, free_count

    def count_major_diagonal(self, x: int, y: int, player: str) -> (int, int, int):
        left_up, free_left_up = self.count_in_direction(x, y, move_left_up, player)
        right_down, free_right_down = self.count_in_direction(x, y, move_right_down, player)
        return left_up + right_down + 1, free_left_up, free_right_down

    def count_minor_diagonal(self, x: int, y: int, player: str) -> (int, int, int):
        left_down, free_left_down = self.count_in_direction(x, y, move_left_down, player)
        right_up, free_right_up = self.count_in_direction(x, y, move_right_up, player)
        return left_down + right_up + 1, free_left_down, free_right_up

    def count_horizontal(self, x: int, y: int, player: str) -> (int, int, int):
        right, free_right = self.count_in_direction(x, y, move_right, player)
        left, free_left = self.count_in_direction(x, y, move_left, player)
        return right + left + 1, free_right, free_left

    def count_vertical(self, x: int, y: int, player: str) -> (int, int, int):
        up, free_up = self.count_in_direction(x, y, move_up, player)
        down, free_down = self.count_in_direction(x, y, move_down, player)
        return up + down + 1, free_up, free_down

    # </editor-fold>

    def log_board(self, guides: bool = True, colors=True):
        dash_line_length = self.board.shape[1] * 4 + 1
        dash_line = f'{"    " if guides else ""}{"-" * dash_line_length}'
        if guides:
            logging.info("     " + " ".join(f" {x} " for x in range(1, 8)))
        logging.info(dash_line)
        for idx, line in enumerate(self.board):
            formatted_line = f'{f" {6 - idx}  " if guides else ""}|{"|".join([f" {x} " for x in line])}| '
            logging.info(formatted_line)
        logging.info(dash_line)

    def copy(self):
        board_copy = ConnectFourBoard()
        board_copy.board = self.board.copy()

        return board_copy
