import numpy as np


class Board:
    def __init__(self, size: (int, int), data_type: np.dtype = int, zero=0):
        self.board = np.ndarray(size, dtype=data_type)
        self.zero = zero

    def copy(self):
        board_copy = Board(self.board.shape, self.board.dtype, self.zero)
        board_copy.board = self.board.copy()
        return board_copy
