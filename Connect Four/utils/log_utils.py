import os


class Logger:
    def __init__(self, log_file_path):
        self.file_path = log_file_path
        if os.path.exists(log_file_path):
            os.remove(log_file_path)

    def log(self, msg: str, alt_msg: str = None):
        print(msg)
        with open(self.file_path, "a") as log_file:
            log_file.write(msg if alt_msg is None else alt_msg)
            log_file.write("\n")
