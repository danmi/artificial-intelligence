from typing import Callable, Any


def run_input_loop(msg: str, verify_func: Callable[[Any], bool], error_msg: str) -> str:
    i = None
    while not verify_func(i):
        i = input(msg + " ")
        if error_msg is not None and not verify_func(i):
            print(error_msg)
    return i
