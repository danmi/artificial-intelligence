import random


def select_move(costs: list) -> int:
    """
    Select a move based on the following heuristic: Select move with the lowest cost. If there are multiple options with
    the same cost, select one randomly.
    :param costs: A list containing tuples of (index, cost).
    :return: One of the indices with the minimum cost.
    """
    sorted_costs = sorted(costs, key=lambda x: -x[1])
    max_cost = sorted_costs[0][1]
    minimum_cost_indices = [idx for idx, cost in sorted_costs if cost == max_cost]
    return random.choice(minimum_cost_indices)
    # return 4


def is_n_not_blocked_in_both_direction(n: int, count: int, a: int, b: int) -> bool:
    # Look for 3 in a row that are not blocked at both direction:
    if count == n and a >= 4 - n and b >= 4 - n:
        return True
    return False


def boost_n_free_in_both_direction(n: int, c: dict, gains: dict, boost: float) -> None:
    for direction, counts in c.items():
        if is_n_not_blocked_in_both_direction(n, *counts):
            gains[direction] = gains.get(direction, 0) + boost
