import gc
import sys
import traceback
import logging
from heuristics import *
from agents.ai_agent import *
from game_runner import GameRunner
from agents.human_player import HumanPlayer
from utils.general_utils import run_input_loop
# from heuristics.min_max_heuristic import MinMaxHeuristic
from utils.connect_four_log_formatter import ConnectFourColorFormatter

player_name = None

root_logger = logging.getLogger()
logger_file_handler = None
logger_console_handler = None

SCORES = {
    "three_both_directions": 1000,
    "two_both_directions": 300,
    "one_both_directions": 100,
    "balance_boost": 20,
    "7-n": 80,
    "6-n": 60,
    "5-n": 40,
    "4-n": 20,
    "agent_coef": 1,
    "opponent_coef": 1
}


def pretty(d, indent=0):
    for key, value in d.items():
        print('\t' * indent + str(key))
        if isinstance(value, dict):
            pretty(value, indent + 1)
        else:
            print('\t' * (indent + 1) + str(value))


def run_until_win(game_runner: GameRunner, win_player: str, first_player: Player, second_player: Player) -> None:
    winner = None
    while winner != win_player:
        winner = game_runner.run_single_game()
        logging.info(f'{first_player.name} moves {first_player.moves_history}')
        logging.info(f'{second_player.name} moves {second_player.moves_history}')
        first_player.reset()
        second_player.reset()
    print(winner)


def run_n_games(board: ConnectFourBoard, first_player: Player, second_player: Player):
    n = run_input_loop("How many games do you want to run?", is_positive_integer, "Please enter a positive integer")
    print("Note: Only a summary of the result will be shown. The full games will be logged in 'connect_four.log'")

    n = int(n)

    set_loggers_handlers(logger_file_handler)

    game_runner = GameRunner(board, first_player, second_player)

    results = game_runner.run_games_batch(n)
    first_player_success_first = n / 2 - results[f'{first_player.name} First'][f'{second_player.name}']
    first_player_success_second = n / 2 - results[f'{second_player.name} First'][f'{second_player.name}']
    first_total_success = first_player_success_first + first_player_success_second

    second_player_success_first = n / 2 - results[f'{second_player.name} First'][f'{first_player.name}']
    second_player_success_second = n / 2 - results[f'{first_player.name} First'][f'{first_player.name}']
    second_total_success = second_player_success_first + second_player_success_second

    print(f'{first_player.name} success rate when first: {first_player_success_first / (n / 2)}')
    print(f'{first_player.name} success rate when second: {first_player_success_second / (n / 2)}')
    print(f'{first_player.name} total success rate: {first_total_success / n}')

    print(f'{second_player.name} success rate when first: {second_player_success_first / (n / 2)}')
    print(f'{second_player.name} success rate when second: {second_player_success_second / (n / 2)}')
    print(f'{second_player.name} total Success rate: {second_total_success / n}')

    print('Detailed statistics:')
    for key, value in results.items():
        print(key)
        for winner, count in value.items():
            print(f'\t{winner}: {count}')


def set_logging(console=False, file=True):
    root_logger.setLevel(logging.INFO)

    log_formatter = logging.Formatter('%(asctime)s %(message)s')
    colored_log_formatter = ConnectFourColorFormatter()

    global logger_file_handler
    logger_file_handler = logging.FileHandler('connect_four.log', mode="w+")
    logger_file_handler.setFormatter(log_formatter)

    global logger_console_handler
    logger_console_handler = logging.StreamHandler(sys.stdout)
    logger_console_handler.setFormatter(colored_log_formatter)


def set_loggers_handlers(*handlers):
    reset_loggers_handler()
    for handler in handlers:
        root_logger.addHandler(handler)


def reset_loggers_handler():
    for handler in root_logger.handlers:
        root_logger.removeHandler(handler)
    return root_logger


def welcome_prompt():
    # os.system('cls')
    print(f"Welcome {player_name}! What would you like to do?")
    print("1. Play against the ai agent")
    print("2. Play against a random agent (why would anybody want to do that?!")
    print("3. Watch the ai agent playing against a random agent")
    print("4. Watch the ai agent playing against himself")
    print("5. Let the ai agent play {n} games against a random agent")
    print("6. Let the ai agent play {n} games against himself")
    print("7. Control the universe! HA HA HA (evil laughter)")
    print("8. Exit")


def create_ai_players(board: ConnectFourBoard, random=False) -> (AIAgent, AIAgent):
    player1_heuristic = StandardHeuristic(SCORES)
    # player1_heuristic = MinMaxHeuristic(SCORES, 0)
    player2_heuristic = ConstantHeuristic(7) if random else player1_heuristic
    player1 = AIAgent('AI Agent' if random else 'Player 1', board, 'X', 'O', player1_heuristic)
    player2 = AIAgent('Random Agent' if random else 'Player 2', board, 'O', 'X', player2_heuristic)
    return player1, player2


def create_human_and_ai_players(board: ConnectFourBoard, human_first: bool, random: bool) -> (HumanPlayer, AIAgent):
    human_agent = HumanPlayer(player_name, board, 'X' if human_first else 'O')
    heuristic = ConstantHeuristic(7) if random else StandardHeuristic(SCORES)
    ai_agent = AIAgent('Random Agent' if random else 'AI Agent', board, 'O' if human_first else 'X',
                       'X' if human_first else 'O', heuristic)
    return human_agent, ai_agent


def is_positive_integer(value: str) -> bool:
    try:
        if value is not None and int(value) > 0:
            return True
        return False
    except ValueError:
        return False


def run_n_games_against_random():
    board = ConnectFourBoard()

    player1, player2 = create_ai_players(board, True)

    run_n_games(board, player1, player2)


def run_n_games_against_ai():
    board = ConnectFourBoard()

    player1, player2 = create_ai_players(board, False)

    run_n_games(board, player1, player2)


def ai_vs_ai():
    initialize_single_auto_game(False)


def ai_vs_random():
    initialize_single_auto_game(True)


def initialize_single_auto_game(random_agent: bool):
    board = ConnectFourBoard()

    player1, player2 = create_ai_players(board, random_agent)

    set_loggers_handlers(logger_console_handler, logger_file_handler)

    game_runner = GameRunner(board, player1, player2)

    game_runner.run_single_game()


def human_vs_random():
    initialize_single_manual_game(True)


def human_vs_ai():
    initialize_single_manual_game(False)


def initialize_single_manual_game(random_agent: bool):
    is_human_first = run_input_loop('Do you want to be first? (y/n)', lambda x: x in ['y', 'n'],
                                    "Please select 'y' or 'n' ")
    board = ConnectFourBoard()

    is_human_first = True if is_human_first == 'y' else False
    human_player, ai_agent = create_human_and_ai_players(board, is_human_first, random_agent)

    set_loggers_handlers(logger_console_handler, logger_file_handler)

    game_runner = GameRunner(board, human_player if is_human_first else ai_agent,
                             ai_agent if is_human_first else human_player)

    game_runner.run_single_game()


def control_universe():
    raise NotImplementedError


if __name__ == "__main__":

    set_logging()

    player_name = input("Hi, what's your name? ")
    if player_name == "":
        player_name = "someone"

    options = {
        "1": human_vs_ai,
        "2": human_vs_random,
        "3": ai_vs_random,
        "4": ai_vs_ai,
        "5": run_n_games_against_random,
        "6": run_n_games_against_ai,
        "7": control_universe,
        "8": sys.exit
    }

    while True:
        selection = None
        while selection not in options.keys():
            welcome_prompt()
            selection = input()
            if selection not in options.keys():
                print("Please select an option between 1 to 8")
        try:
            print("Note: All the results are also saved in 'connect_four.log'")
            options[selection]()
            gc.collect()
        except NotImplementedError as e:
            print(type(e).__name__)
        except Exception as e:
            print("Error!")
            print(type(e).__name__)
            print(str(traceback.format_exc()))

        input("Press Enter to start over...")
