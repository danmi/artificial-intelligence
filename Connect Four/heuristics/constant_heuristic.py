from boards.connect_four_board import ConnectFourBoard
from heuristics.heuristic import Heuristic


class ConstantHeuristic(Heuristic):
    def __init__(self, constance_score: float):
        self.constant_score = constance_score

    def calculate_heuristic(self, board: ConnectFourBoard = None, column: int = None, **args):
        return self.constant_score
