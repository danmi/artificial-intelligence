import math
from typing import Dict
from heuristics import StandardHeuristic
from boards.connect_four_board import ConnectFourBoard


class MinMaxHeuristic(StandardHeuristic):
    def __init__(self, scores: Dict[str, float], depth: int):
        super().__init__(scores)
        self.depth = depth

    def calculate_minimax_score(self, board: ConnectFourBoard, column: int, player: str, opponent: str,
                                iteration: int = 0, minmax=max) -> float:
        if board.is_column_full(column):
            return -1

        if iteration == self.depth:
            return self.calculate_total_score(board.copy(), column, player, opponent)

        starting_value = -math.inf if minmax == max else math.inf

        board.add_to_column(column, player)

        next_func = min if minmax == max else max

        scores = [starting_value]
        for column in range(7):
            if board.is_column_full(column):
                score = -1
            else:
                score = self.calculate_minimax_score(board.copy(), column, opponent, player, iteration + 1, next_func)
            if score > 0:
                scores.append(score)
        return minmax(scores)

    def calculate_heuristic(self, board: ConnectFourBoard = None, column: int = None, **args):
        return self.calculate_minimax_score(board.copy(), column, args["player"], args["opponent"])
