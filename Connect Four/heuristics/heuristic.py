from abc import ABC, abstractmethod
from boards.connect_four_board import ConnectFourBoard


class Heuristic(ABC):

    @abstractmethod
    def calculate_heuristic(self, board: ConnectFourBoard = None, column: int = None, **args):
        pass
