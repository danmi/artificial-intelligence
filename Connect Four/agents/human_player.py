from agents.player import Player
from boards.connect_four_board import *
from utils.general_utils import run_input_loop


def try_parse_column(value: str) -> bool:
    try:
        if value is not None and 1 <= int(value) <= 7:
            return True
        return False
    except ValueError:
        return False


class HumanPlayer(Player):

    def reset(self) -> None:
        pass

    def __init__(self, name: str, board: ConnectFourBoard, player: str):
        super().__init__(name, board, player)

    def select_move(self) -> int:
        selection = run_input_loop("Please select a column:", try_parse_column,
                                   "You must enter a column number between 1 to 7!")
        return int(selection)
