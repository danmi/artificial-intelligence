from agents import AIAgent
from heuristics.heuristic import Heuristic
from utils import select_best_move
from boards.connect_four_board import ConnectFourBoard


class DebuggingAgent(AIAgent):
    def __init__(self, name: str, board: ConnectFourBoard, player: str, opponent: str, moves: list,
                 heuristic: Heuristic):
        super().__init__(name, board, player, opponent, heuristic)
        self.moves = moves
        self.moves_count = 0
        self.heuristic = heuristic

    def reset(self):
        super().reset()
        self.moves_count = 0

    def select_move(self) -> int:
        if self.moves_count < len(self.moves):
            next_move = self.moves[self.moves_count]
            self.moves_count += 1
        else:
            gains = self.calculate_scores()
            next_move = select_best_move(gains) + 1
        return next_move
