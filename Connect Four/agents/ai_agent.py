from agents.player import Player
from utils.connect_four_utils import *
from boards.connect_four_board import *
from heuristics.heuristic import Heuristic


class AIAgent(Player):
    def __init__(self, name: str, board: ConnectFourBoard, player: str, opponent: str, heuristic: Heuristic):
        super().__init__(name, board, player)
        self.opponent = opponent
        self.heuristic = heuristic

    def reset(self):
        super().reset()
        self.board.reset()

    def calculate_scores(self):
        """
        Calculates the score for each column. If the column has free spaces then the score is calculated using the
        heuristic, else the column will be dropped from the scores list.
        :return: A list containing tuples of column number and the score for this columns.
        """
        scores = []
        for column in range(7):
            if self.board.is_column_full(column):
                score = -1
            else:
                score = self.heuristic.calculate_heuristic(self.board, column, player=self.player, opponent=self.opponent)
            if score > 0:
                scores.append((column, score))
        self.scores_history.append(scores)
        return scores

    def select_move(self) -> int:
        scores = self.calculate_scores()
        # Just converting from the array index (that starts with 0) and the columns index
        next_move = select_best_move(scores) + 1
        self.moves_history.append(next_move)
        return next_move
