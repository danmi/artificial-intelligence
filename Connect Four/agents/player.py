from abc import ABC, abstractmethod

from boards.board import Board


class Player(ABC):

    def __init__(self, name: str, board: Board, player: str):
        self.name = name
        self.board = board
        self.player = player
        self.scores_history = []
        self.moves_history = []

    @abstractmethod
    def move(self) -> int:
        pass

    @abstractmethod
    def reset(self) -> None:
        self.scores_history = []
        self.moves_history = []
